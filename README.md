# README

Link tham khảo: https://www.sitepoint.com/rails-model-caching-redis/

Một số lưu ý

 1. Sử lý khi redis lỗi
    Chúng ta cần phải bắt exception cho trường hợp như này
```
    # posts_controller.rb
    def fetch_post
      begin
        posts = $redis.get "posts"

        if posts.nil?
          posts = Post.all.to_json
          $redis.set "posts", posts
        end
        posts = JSON.load posts
      rescue => error
        puts error.inspect
        posts = Post.all
      end
      posts
    end
```


  2. Dữ liệu sau khi update hoặc delete
    Trong trường hợp dữ liệu sau khi update hoặc bị xóa thì trong redis dữ liệu vẫn chưa được cập nhật lại. Vì thế chúng ta cần cập nhật lại dữ liệu trong redis khi gặp trường hợp này

```
    # post.rb
    class Post < ApplicationRecord
      after_save :clear_cache
      after_destroy :clear_cache

      private
      def clear_cache
        $redis.del "posts"
      end
    end
```    
