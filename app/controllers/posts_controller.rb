# app/controllers/posts_controller.rb
class PostsController < ApplicationController
  def index
    @posts = fetch_post
    
    respond_to do |format|
      format.json {render json: @posts, status: :ok}
    end
  end
  
  private
  
  def fetch_post
      posts = $redis.get "posts"

      if posts.nil?
        posts = Post.all.to_json
        $redis.set "posts", posts
      end
      
      JSON.load posts
   end
end
